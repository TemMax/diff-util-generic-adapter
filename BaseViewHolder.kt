import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.extensions.LayoutContainer


abstract class BaseViewHolder<in T>(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {

    override val containerView: View
        get() = itemView

    var clickListener: BaseAdapter.IRecyclerViewClickListener? = null
    var longClickListener: BaseAdapter.IRecyclerViewLongClickListener? = null

    abstract fun bind(item: T, payloads: List<Any>)

    abstract fun flush()

}