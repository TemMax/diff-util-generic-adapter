import android.view.View


class HoldersFactory {

    fun holder(type: Int, view: View): BaseViewHolder<*> {
        return when (type) {
            else                                     -> throw IllegalArgumentException("Illegal view type")
        }
    }

}