import android.content.Context
import android.support.annotation.LayoutRes
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.channels.Channel
import kotlinx.coroutines.experimental.channels.actor


abstract class BaseAdapter<M : ViewModel> : RecyclerView.Adapter<BaseViewHolder<ViewModel>>() {

    interface IRecyclerViewClickListener {
        fun onItemClick(position: Int)
    }

    interface IRecyclerViewLongClickListener {
        fun onItemLongClick(position: Int)
    }

    interface DataChangedListener {
        fun onChanged(itemCount: Int)
    }

    protected lateinit var c: Context
    private lateinit var rv: RecyclerView
    private var clickListener: IRecyclerViewClickListener? = null
    private var longClickListener: IRecyclerViewLongClickListener? = null
    var changedListener: DataChangedListener? = null

    private val holdersFactory = HoldersFactory()

    @Volatile
    var data: List<M> = listOf()
        private set

    private val diffCallback by lazy(LazyThreadSafetyMode.NONE) { diffCallback() }
    private val eventActor = actor<List<M>>(context = CommonPool, capacity = Channel.CONFLATED) { for (list in channel) internalUpdate(list) }

    fun setClickListener(actionOnClick: (M, Int) -> Unit) {
        clickListener = object : IRecyclerViewClickListener {
            override fun onItemClick(position: Int) {
                actionOnClick(data[position], position)
            }
        }
    }

    fun setLongClickListener(actionOnLongClick: (M, Int) -> Unit) {
        longClickListener = object : IRecyclerViewLongClickListener {
            override fun onItemLongClick(position: Int) {
                actionOnLongClick(data[position], position)
            }
        }
    }

    override fun getItemViewType(position: Int) = data[position].type

    @Suppress("UNCHECKED_CAST")
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<ViewModel> = holdersFactory.holder(viewType, inflate(viewType, parent)) as BaseViewHolder<ViewModel>

    override fun onBindViewHolder(holder: BaseViewHolder<ViewModel>, position: Int) {
        holder.clickListener = clickListener
        holder.longClickListener = longClickListener
        holder.flush()
        holder.bind(get(position), listOf())
    }

    override fun onBindViewHolder(holder: BaseViewHolder<ViewModel>, position: Int, payloads: List<Any>) {
        holder.clickListener = clickListener
        holder.longClickListener = longClickListener
        holder.flush()
        holder.bind(get(position), payloads)
    }

    fun update(list: List<M>) {
        eventActor.offer(list)
        changedListener?.onChanged(list.size)
    }

    abstract fun diffCallback(): BaseDiffCallback<M>

    private suspend fun internalUpdate(list: List<M>) {
        val result = DiffUtil.calculateDiff(diffCallback.apply {
            oldList = data
            newList = list
        }, false)
        async(UI) {
            data = list
            result.dispatchUpdatesTo(this@BaseAdapter)
        }.join()
    }

    operator fun get(position: Int) = data[position]

    override fun getItemCount() = data.size

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        val resId = R.anim.layoyt_animation_fade_in
        val animation = AnimationUtils.loadLayoutAnimation(recyclerView.context, resId)
        recyclerView.layoutAnimation = animation

        super.onAttachedToRecyclerView(recyclerView)

        this.c = recyclerView.context as Context
        this.rv = recyclerView
    }

    private fun inflate(@LayoutRes layout_id: Int, parent: ViewGroup? = null): View =
            LayoutInflater.from(c).inflate(layout_id, parent, false)

}