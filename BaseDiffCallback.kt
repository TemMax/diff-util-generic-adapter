import android.support.v7.util.DiffUtil


abstract class BaseDiffCallback<M> : DiffUtil.Callback() {

    lateinit var oldList: List<M>
    lateinit var newList: List<M>

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

}